#!/bin/sh
# -*- mode: shell-script-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
#
# Copyright (C) 2016 Assured Information Security, Inc. <pattersonc@ainfosec.com>
# Copyright (C) 2012-13 O.S. Systems Software LTDA.
# Authored-by:  Otavio Salvador <otavio@ossystems.com.br>
# Adopted to Angstrom:  Khem Raj <raj.khem@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

if [ "$(whoami)" = "root" ]; then
    echo "ERROR: do not use the BSP as root. Exiting..."
    return
fi

OEROOT="`pwd`"
cd "$OEROOT"
if [ -n "$ZSH_VERSION" ]; then
    setopt sh_word_split
    setopt clobber
elif [ -n "$BASH_VERSION" ]; then
    set +o noclobber
fi

if [ -z "${SDKMACHINE}" ]; then
    SDKMACHINE='x86_64'
fi

MANIFESTS="${OEROOT}"/.repo/manifests

# Clean up PATH, because if it includes tokens to current directories somehow,
# wrong binaries can be used instead of the expected ones during task execution
export PATH="`echo ${PATH} | sed 's/\(:.\|:\)*:/:/g;s/^.\?://;s/:.\?$//'`"
export PATH="${OEROOT}"/sources/openembedded-core/scripts:"${OEROOT}"/sources/bitbake/bin:"${OEROOT}"/.repo/repo:"${PATH}"
#remove duplicate path entries
export PATH="`echo $PATH | awk -F: '{for (i=1;i<=NF;i++) { if ( !x[$i]++ ) printf("%s:",$i); }}' | sed 's/:$//'`"
# Make sure Bitbake doesn't filter out the following variables from our
# environment.
export BB_ENV_EXTRAWHITE="MACHINE DISTRO TCLIBC TCMODE GIT_PROXY_COMMAND http_proxy ftp_proxy https_proxy all_proxy ALL_PROXY no_proxy SSH_AGENT_PID SSH_AUTH_SOCK BB_SRCREV_POLICY SDKMACHINE BB_NUMBER_THREADS REDFIELD_SOURCES_URL REDFIELD_BRANCH"

mkdir -p conf
if [ -f "${OEROOT}/conf/auto.conf" ]; then
    oldmach=`fgrep "^MACHINE" "${OEROOT}/conf/auto.conf" | sed -e 's%^MACHINE ?= %%' | sed -e 's/^"//'  -e 's/"$//'`
fi
if [ -e conf/checksum -a "${MACHINE}" = "$oldmach" ]
then
    sha512sum --quiet -c conf/checksum > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
       return
    fi
fi

# evaluate new checksum and regenerate the conf files
sha512sum "${MANIFESTS}"/setup-environment-internal 2>&1 > conf/checksum

DISTRO=$(grep -w DISTRO conf/local.conf | grep -v '^#' | awk -F\" '{print $2}')
export DISTRO_DIRNAME=`echo ${DISTRO} | sed 's#[.-]#_#g'`

cat > conf/auto.conf <<EOF
MACHINE ?= "${MACHINE}"
SDKMACHINE ?= "${SDKMACHINE}"
EOF
if [ ! -e conf/site.conf ]; then
    cat > conf/site.conf <<_EOF

SCONF_VERSION = "1"

# Where to store sources
DL_DIR = "${OEROOT}/build/downloads"

# Where to save shared state
SSTATE_DIR = "${OEROOT}/build/sstate-cache"

# Which files do we want to parse:
BBFILES ?= "${OEROOT}/sources/openembedded-core/meta/recipes-*/*/*.bb"

TMPDIR = "${OEROOT}/build/tmp-${DISTRO_DIRNAME}"

# Go through the Firewall
#HTTP_PROXY        = "http://${PROXYHOST}:${PROXYPORT}/"

# Default sstate mirror for redfield
SSTATE_MIRRORS = "file://.* https://redfield.dev/redfield/sstate-cache/PATH"

_EOF
fi

export DEPLOY_DIR_IMAGE="build/tmp-${DISTRO_DIRNAME}-glibc/deploy/images/${MACHINE}"

cat <<EOF

Your build environemnt has been configured with:

    MACHINE=${MACHINE}
    SDKMACHINE=${SDKMACHINE}
    DISTRO=${DISTRO}

You can now run 'bitbake <target>'

Some of common targets are:
    dom0-image
    ndvm-image

You may also want to synchronize your repositories:
    repo sync

Or perhaps create a new working branch:
    repo start <branch-name> --all

build all images:
    bitbake dom0-image ndvm-image
EOF
