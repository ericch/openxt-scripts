include .repo/manifests/distro.mk
DISTRO ?= openxt 
MACHINE ?= xenclient-dom0-image 
DEPLOY_SRCDIR ?= build/tmp-$(DISTRO)-glibc/deploy/images/$(MACHINE)
DESTDIR ?= /storage

IMAGES ?= \
	xenclient-dom0-image \
	xenclient-stubdomain-initramfs-image \
	xenclient-ndvm-image \
	xenclient-uivm-image \
	xenclient-installer-image \
	xenclient-syncvm-image

IMAGES_FS := $(foreach IMAGE,$(IMAGES),$(DEPLOY_SRCDIR)/$(IMAGE)-$(MACHINE).ext4)
XEN := build/tmp-$(DISTRO)-glibc/deploy/images/$(MACHINE)/xen-$(MACHINE).gz
VMLINUZ := build/tmp-$(DISTRO)-glibc/deploy/images/$(MACHINE)/bzImage-$(MACHINE).bin

.PHONY: all
all:
	./bb-wrapper --continue $(IMAGES)

.PHONY: dom0
dom0:
	MACHINE=xenclient-dom0 ./bb-wrapper -k xenclient-dom0-image

.PHONY: stubdom
stubdom:
	MACHINE=xenclient-stubdomain ./bb-wrapper -k xenclient-stubdomain-initramfs-image

.PHONY: installer
installer:
	MACHINE=xenclient-installer-image ./bb-wrapper xenclient-installer-image

.PHONY: installer-net-image
installer-net-image:
	./bb-wrapper installer-net-image

.PHONY: ndvm
ndvm:
	./bb-wrapper ndvm-image

.PHONY: clean
clean:
	rm -rf build cache

.PHONY: install
install:
	mkdir -p $(DESTDIR)
	install -m 0755 $(IMAGES_FS) $(DESTDIR)/
	install -m 0755 $(XEN) $(DESTDIR)/
	install -m 0755 $(VMLINUZ) $(DESTDIR)/

.PHONY: fetch
fetch:
	MACHINE=xenclient-dom0 ./bb-wrapper --runall=fetch xenclient-dom0-image
	MACHINE=xenclient-ndvm ./bb-wrapper --runall=fetch xenclient-ndvm-image
	MACHINE=xenclient-uivm ./bb-wrapper --runall=fetch xenclient-uivm-image
	MACHINE=xenclient-stubdomain ./bb-wrapper --runall=fetch xenclient-stubdomain-initramfs-image

.PHONY: generate-certs
generate-certs:
	mkdir certs 
	openssl genrsa -out certs/prod-cakey.pem 2048
	openssl genrsa -out certs/dev-cakey.pem 2048
	openssl req -new -x509 -key certs/prod-cakey.pem -out certs/prod-cacert.pem -days 1095
	openssl req -new -x509 -key certs/dev-cakey.pem -out certs/dev-cacert.pem -days 1095


.PHONY: branch-create
branch-create:
	repo start $(BRANCH) --all
	repo checkout $(BRANCH)
	repo forall -c git commit --allow-empty -m "Created release branch for $(BRANCH)"
	repo forall -c 'bash -c "git push $$REPO_REMOTE $(BRANCH):$(BRANCH)"'
	cd .repo/manifests && \
	echo $$PWD && \
	sed 's/master/$(BRANCH)/g' default.xml > $(BRANCH).xml && \
	git add $(BRANCH).xml && \
	git commit -s -m "Create manifest for release branch $(BRANCH)" && \
	git push origin default:master
	repo init -m $(BRANCH).xml

.PHONY: branch-delete
branch-delete:
	#repo forall -c 'bash -c "git push $$REPO_REMOTE :$(BRANCH)"'
	echo "Branch delete is disabled by default to prevent accidental deletion. If you intend to delete $(BRANCH), uncomment the repo forall command in the branch-delete target of the Makefile."

.PHONY: branch-tag-rc
branch-tag-rc:
	repo forall -c git commit --allow-empty -m "Tag release candidate: $(BRANCH)-rc$(RC_NUMBER)"
	repo forall -c 'bash -c "git tag $(BRANCH)-rc$(RC_NUMBER)"'
	repo forall -c 'bash -c "git push $$REPO_REMOTE $(BRANCH)-rc$(RC_NUMBER)"'

.PHONY: branch-tag-release
branch-tag-release:
	repo forall -c git commit --allow-empty -m "Tag release: $(BRANCH)-v$(MAJOR).$(MINOR)"
	repo forall -c 'bash -c "git tag $(BRANCH)-v$(MAJOR).$(MINOR)"'
	repo forall -c 'bash -c "git push $$REPO_REMOTE $(BRANCH)-v$(MAJOR).$(MINOR)"'
